import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import DatePicker from 'react-native-datepicker';

export default class App extends React.Component {

    constructor(props){
      super(props)
      this.state = {myDate : "3000-01-01"}
    }
  updateState(date){
    this.setState({myDate : date});
    alert("The new Selected Date is : " + this.state.myDate);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>My DatePicker</Text>
        <DatePicker
        style={{width: 200}}
        date={this.state.myDate}
        mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        onDateChange={(date) => {this.updateState(date)}}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title:{
    fontSize: 40,
    fontWeight: 'bold',
  },
});
